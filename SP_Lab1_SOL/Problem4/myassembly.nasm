global _start

SECTION .text
_start:
mov rax,0 ;syscall # of read()
mov rdi,0 ;1st arg is file descriptor
mov rsi,msg ;2nd arg is address of string where read content will be saved
mov rdx,100;3rd arg is no of bytes to read before truncating the data. If the data to be read is smaller than mentioned bytes, all data is saved in the buffer.
syscall

mov r15,rax
mov rax,1 ;syscall # of write()
mov rdi,1 ;1st arg is file descriptor
mov rsi,msg ;2nd arg is address of string
mov rdx,r15;3rd arg is length of string
syscall

mov rax,60 ;system call number of exit()
mov rdi,54 ;1st arg to exit()
syscall

SECTION .data
msg: db "", 0Ah, 0h
len_msg: equ $ - msg 
