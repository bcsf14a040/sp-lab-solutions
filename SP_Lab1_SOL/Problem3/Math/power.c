
/* Power function */
long long power(int base, int exponent)
{
    long long  result = 1;
    while (exponent != 0)
    {
        result *= base;
        --exponent;
    }

    return result;
}
